import Vue from 'vue'
import Filter from 'bad-words'
//  Helper
Vue.filter('lower', (val) => {
  return val.toUpperCase()
})

Vue.filter('upper', (val) => {
  return val.toUpperCase()
})
Vue.filter('capitalize', (val) => {
  return val.replace(/\b\w/g, (l) => l.toUpperCase())
})

Vue.filter('slugify', (val) => {
  // For more information, visit https://www.npmjs.com/package/slug
  var slug = require('slug')
  return slug(val)
})
Vue.filter('profanity', (val) => {
  const filter = new Filter()
  const profanityList = ['shit', 'suck', 'sucks', 'gay', 'gei']
  filter.addWords(...profanityList)
  return filter.clean(val)
})
