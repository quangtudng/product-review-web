import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import { required, email } from 'vee-validate/dist/rules'
extend('required', {
  ...required,
  message: 'error.fieldCannotBeEmpty'
})
extend('email', {
  ...email,
  message: 'error.InvalidEmail'
})
extend('min', {
  validate(value, { length }) {
    return value.length >= length
  },
  params: ['length'],
  message: `error.FieldRequired{length}Characters`
})
extend('max', {
  validate(value, { length }) {
    return value.length <= length
  },
  params: ['length'],
  message: `error.FieldMax{length}Characters`
})
extend('between', {
  validate(value, { min, max }) {
    return parseInt(value) >= parseInt(min) && parseInt(value) <= parseInt(max)
  },
  params: ['min', 'max'],
  message: `error.fieldBetween{min}And{max}`
})
extend('numeric', {
  validate(value) {
    return String(value).match(/^[+-]?\d+(\.\d+)?$/)
  },
  message: 'error.mustBeNumeric'
})
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
