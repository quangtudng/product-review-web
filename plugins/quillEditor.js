import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.snow.css' // for snow theme

Vue.use(VueQuillEditor)
