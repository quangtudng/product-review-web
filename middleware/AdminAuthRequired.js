export default ({ store, redirect }) => {
  try {
    if (
      store.state.user.currentUser.roleName != 'administrator' &&
      store.state.user.currentUser.roleName != 'partner'
    ) {
      store.dispatch('logout')
      redirect('/admin/auth/login')
    }
  } catch (error) {
    store.dispatch('logout')
    redirect('/admin/auth/login')
  }
}
