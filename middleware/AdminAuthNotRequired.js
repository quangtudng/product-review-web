export default function ({ store, redirect }) {
  if (store.state.user.currentUser) {
    if (
      store.state.user.currentUser.roleName == 'administrator' ||
      store.state.user.currentUser.roleName == 'partner'
    )
      return redirect('/admin')
  }
}
