export default ({ store, redirect }) => {
  if (store.state.user.currentUser.roleName == 'partner') {
    redirect('/admin/users')
  }
}
