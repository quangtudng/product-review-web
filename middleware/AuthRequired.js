export default ({ store, redirect }) => {
  if (store.state.user.currentUser == null) {
    redirect('/app/auth/login')
  }
}
