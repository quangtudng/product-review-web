import Vue from 'vue'
Vue.mixin({
  methods: {
    /*
      type: String ( error or success )
      title: String
      message: String or Array
    */
    notifyToast(type, title, message) {
      if (type == 'success') {
        this.$bvToast.toast(this.$t(message), {
          title: this.$t(title),
          variant: 'success',
          autoHideDelay: 2000,
          solid: true
        })
      }
      if (type == 'error') {
        // Array of messages
        if (Array.isArray(message)) {
          let formattedMessageArray = []
          message.forEach((m) => {
            formattedMessageArray.push(
              this.$parent.$createElement(
                'p',
                { class: ['m-0'] },
                `• ${this.$t(m.msg)}`
              )
            )
          })
          this.$bvToast.toast(formattedMessageArray, {
            title: this.$t(title),
            variant: 'danger',
            autoHideDelay: 2000,
            solid: true
          })
          // Single message
        } else {
          this.$bvToast.toast(this.$t(message), {
            title: this.$t(title),
            variant: 'danger',
            autoHideDelay: 2000,
            solid: true
          })
        }
      }
    }
  }
})
