export default {
  SET_VIEWING(state, payload) {
    state.viewing = payload
  },
  SET_VIEWING_COMPARE(state, payload) {
    state.viewing.compare = payload
  },
  SET_SEARCH_FILTER(state, text) {
    state.query.name = text
  },
  SET_VIEWING_RECOMMENDATION(state, payload) {
    state.viewing.recommendation = payload
  },
  SET_PAGE_FILTER(state, page) {
    state.query.page = page
  },
  SET_LIMIT_FILTER(state, limit) {
    state.query.size = limit
  },
  RESET_FILTER(state) {
    state.query.name = ''
    state.query.page = 1
    state.query.size = 5
  }
}
