export default {
  async fetchDetail({ commit }, payload) {
    const response = await this.$axios.get(`/product/${payload.slug}`)
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data.data)
    } else {
      throw response.data.error
    }
  },
  async fetchList({ state, commit }) {
    const response = await this.$axios.get(
      `/products?name=${encodeURIComponent(state.query.name)}&page=${
        state.query.page
      }&size=${state.query.size}`
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  },
  async fetchCompare({ commit }, payload) {
    const responseOne = await this.$axios.get(`/product/${payload.slugOne}`)
    const responseTwo = await this.$axios.get(`/product/${payload.slugTwo}`)
    if (
      responseOne.data.error == undefined &&
      responseTwo.data.error == undefined
    ) {
      await commit('SET_VIEWING_COMPARE', {
        productOne: responseOne.data.data,
        productTwo: responseTwo.data.data
      })
    } else {
      throw responseOne.data.error
    }
  },
  async fetchListLimit({ commit }) {
    const page = Math.floor(Math.random() * 4) + 1
    const response = await this.$axios.get(
      `/products?name=&page=${page}&size=4&sortBy=point&sort=DESC`
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING_RECOMMENDATION', response.data.data)
    } else {
      throw response.data.error
    }
  },
  async fetchBannedList({ state, commit, rootState }) {
    const response = await this.$axios.get(
      `/products/banned?name=${encodeURIComponent(state.query.name)}&page=${
        state.query.page
      }&size=${state.query.size}`,
      {
        headers: {
          'x-access-token': rootState.user.token
        }
      }
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  }
}
