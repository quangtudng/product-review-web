export default {
  async fetchList({ state, commit }) {
    const response = await this.$axios.get(
      `/users/search?name=${encodeURIComponent(state.query.name)}&page=${
        state.query.page
      }&size=${state.query.size}`,
      {
        headers: {
          'x-access-token': state.token
        }
      }
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  },
  async fetchBannedList({ state, commit }) {
    const response = await this.$axios.get(
      `/users/banned/search?name=${encodeURIComponent(state.query.name)}&page=${
        state.query.page
      }&size=${state.query.size}`,
      {
        headers: {
          'x-access-token': state.token
        }
      }
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  }
}
