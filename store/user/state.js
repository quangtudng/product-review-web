// VueX-State
export default () => ({
  currentUser: null,
  viewing: {},
  query: {
    name: '',
    page: 1,
    size: 5
  },
  token: ''
})
