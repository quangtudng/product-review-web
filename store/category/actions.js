export default {
  // Fetch all children of category
  async fetchChildrenList({ state, commit }) {
    const response = await this.$axios.get(
      `/category/${state.query.category}/products?name=${encodeURIComponent(
        state.query.name
      )}&page=${state.query.page}&size=${state.query.size}&sortBy=${
        state.query.sortBy
      }&sort=${state.query.sort}`
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  },
  // Fetch all category
  async fetchList({ state, commit }) {
    const response = await this.$axios.get(
      `/categories?name=${encodeURIComponent(state.query.name)}&page=${
        state.query.page
      }&size=${state.query.size}`
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  },
  async fetchBannedList({ state, commit, rootState }) {
    const response = await this.$axios.get(
      `/categories/banned?name=${encodeURIComponent(state.query.name)}&page=${
        state.query.page
      }&size=${state.query.size}`,
      {
        headers: {
          'x-access-token': rootState.user.token
        }
      }
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  }
}
