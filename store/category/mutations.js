export default {
  SET_VIEWING(state, payload) {
    state.viewing = payload
  },
  SET_SEARCH_FILTER(state, text) {
    state.query.name = text
  },
  SET_PAGE_FILTER(state, page) {
    state.query.page = page
  },
  SET_LIMIT_FILTER(state, limit) {
    state.query.size = limit
  },
  SET_CATEGORY_FILTER(state, category) {
    state.query.category = category
  },
  SET_SORT_FILTER(state, sortFilter) {
    if (sortFilter != '') {
      const split = sortFilter.split('-')
      state.query.sortBy = split[0]
      state.query.sort = split[1]
    } else {
      state.query.sortBy = ''
      state.query.sort = ''
    }
  },
  RESET_FILTER(state) {
    state.query.name = ''
    state.query.page = 1
    state.query.size = 12
  }
}
