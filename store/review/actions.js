export default {
  async fetchChildrenList({ commit }, payload) {
    const response = await this.$axios.get(
      `/reviews/${payload.slug}?page=${payload.page}&size=${payload.limit}&sortBy=createdAt&sort=DESC`
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  },
  async add({ rootState }, payload) {
    const response = await this.$axios.post(
      `/review/${payload.productID}`,
      payload,
      {
        headers: {
          'x-access-token': rootState.user.token
        }
      }
    )
    if (response.data.error !== undefined) {
      throw response.data.error
    }
  },
  async delete({ rootState }, payload) {
    const response = await this.$axios.delete(`/review/${payload}`, {
      headers: {
        'x-access-token': rootState.user.token
      }
    })
    if (response.data.error !== undefined) {
      throw response.data.error
    }
  },
  async edit({ rootState }, payload) {
    const response = await this.$axios.put(`/review/${payload.id}`, payload, {
      headers: {
        'x-access-token': rootState.user.token
      }
    })
    if (response.data.error !== undefined) {
      throw response.data.error
    }
  },
  async fetchList({ state, commit }) {
    const response = await this.$axios.get(
      `/reviews?name=${encodeURIComponent(state.query.name)}&page=${
        state.query.page
      }&size=${state.query.size}`
    )
    if (response.data.error == undefined) {
      await commit('SET_VIEWING', response.data)
    } else {
      throw response.data.error
    }
  }
}
