// VueX-State
export default () => ({
  viewing: {},
  query: {
    name: '',
    page: 1,
    size: 12,
    sortBy: '',
    sort: ''
  }
})
